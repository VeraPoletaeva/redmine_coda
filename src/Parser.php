<?php

namespace Academy;

use PHPUnit\Runner\Exception;
use Symfony\Component\Yaml\Yaml;

class Parser
{
    private $parsed = '';

    /**
     * Parser constructor.
     * @param string $path
     */
    public function __construct(string $path)
    {
        $this->parsed = Yaml::parseFile($path);
    }

    /**
     * @return array
     */
    public function codaSettings()
    {
        return $this->parsed['Coda'];
    }

    /**
     * @return array
     */
    public function redmineSettings()
    {
        return $this->parsed['Redmine'];
    }
}
