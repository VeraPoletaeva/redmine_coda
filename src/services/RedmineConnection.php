<?php

namespace Academy\Services;

use Redmine;

/**
 * Class RedmineConnection
 * @package Academy\Classes
 */
class RedmineConnection
{
	protected $redminePeriod = 'cd';
	protected $redmineLimit = 10;
	protected $redmineClient = null;
    protected $settings = [];

    /**
     * RedmineConnection constructor.
     * @param string|null $redminePeriod
     * @param int|null $redmineLimit
     */
	public function __construct(array $settings, string $redminePeriod = null, int $redmineLimit = null)
	{
        $this->settings = $settings;

		$this->redminePeriod = $redminePeriod ?? $this->redminePeriod;
		$this->redmineLimit = $redmineLimit ?? $this->redmineLimit;

		$this->redmineClient = new Redmine\Client(
			$this->settings['Link'],
			$this->settings['ConnectKey']
		);
	}

    /**
     * Get rows from redmine
     *
     * @return array
     * @throws \Exception
     */
	public function getData()
	{
		$client = $this->redmineClient;
		$allTimes = $client->time_entry->all([
			'spent_on' => $this->redminePeriod,
			'limit' => $this->redmineLimit
		]);

		if (!$allTimes[0]) {
			return [];
		}

		$currentDate = new \DateTime();
		foreach ($allTimes['time_entries'] as $time) {
			$arTimes[$time['id']] = [
				'ID' => $time['id'],
				'Пользователь' => $time['user']['name'],
				'Списано' => $time['hours'],
				'Проект' => $time['project']['name'],
				'Задача' => $time['issue']['id'] ?? 0,
				'Коммент' => $time['comments'],
				'Списано за' => $time['spent_on'],
				'Создано' => $time['created_on'],
				'Время импорта' => $currentDate->format('Y-m-d H:i:s')
			];
		}

		return $arTimes;
	}
}
