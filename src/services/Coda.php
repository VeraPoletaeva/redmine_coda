<?php

namespace Academy\Services;

use CodaPHP;

class Coda
{
	protected $codaClient = null;
	protected $settings = [];

    /**
     * Coda constructor.
     */
	public function __construct(array $settings)
	{
	    $this->settings = $settings;

		$this->codaClient = new CodaPHP\CodaPHP($this->settings['ConnectKey']);
	}

    /**
     * @param array $data
     * @return bool
     */
	public function addData(array $data)
	{
		$arCodaTimes = $this->codaClient->listRows(
		    $this->settings['InputDocId'],
            $this->settings['InputName']
        );

		foreach ($arCodaTimes['items'] as $item){
			$id = $item['values']['ID'];
			if($id && $data[$id]){
				unset($data[$id]);
			}
		}

		$ins = $this->codaClient->insertRows(
			$this->settings['OutputDocId'],
			$this->settings['OutputName'],
			$data
        );
        
		return $ins;
	}
}
