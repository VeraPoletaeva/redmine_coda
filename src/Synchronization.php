<?php

namespace Academy;

use Academy\Services\Coda;
use Academy\Services\RedmineConnection;

/**
 * Class Synchronization
 * @package Academy\Classes
 */
class Synchronization
{
    /**
     * @return bool
     * @throws \Exception
     */
	public static function redmineToCoda()
	{
	    $settings = new Parser('../config/database.yaml');

        $redmine = new RedmineConnection($settings->redmineSettings());
        $redmineData = $redmine->getData();

        if(!$redmineData) {
            return "Cannot synchronize or no data";
        }

        $coda = new Coda($settings->codaSettings());
        $ins = $coda->addData($redmineData);

        return $ins;
	}
}
