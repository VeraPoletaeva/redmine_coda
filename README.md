Redmine_Coda module
===================
### How to use ###
    Sinchronization::redmineToCoda($pathToConfigFileYaml);

### config.yaml must be like this: ###

    Coda:
        ConnectKey: '12345key67890'
        InputDocId: 'doc12345id'
        OutputTDocId: 'doc12345id'
        InputName: 'Redmine'
        OutputName: 'Redmine'
        
    Redmine:
        Link: 'http://link.ru'
        ConnectKey: '12345key67890'